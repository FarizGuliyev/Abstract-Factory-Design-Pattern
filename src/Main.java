import java.util.Objects;
import java.util.Optional;

public class Main {
    public static void main(String[] args) {
        Object object=FactoryProvider.getFactory("Animal").create("Dog");
        System.out.println(((Dog) object).makeSound());
    }
}